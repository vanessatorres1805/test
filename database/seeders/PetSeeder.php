<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pet;
use App\Models\Owner;
use App\Models\PetType;

class PetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = Owner::find(1);
        $pettype = PetType::find(1);

        $objt = new Pet;
        $objt->name = 'Misy';
        $objt->pet_type_id = $pettype->id;
        $objt->owner_id = $owner->id;
        $objt->save();

        $owner = Owner::find(2);
        $pettype = PetType::find(2);

        $objt = new Pet;
        $objt->name = 'Tommy';
        $objt->pet_type_id = $pettype->id;
        $objt->owner_id = $owner->id;
        $objt->save();

        
        $owner = Owner::find(3);
        $pettype = PetType::find(3);
        
        $objt = new Pet;
        $objt->name = 'Canela';
        $objt->pet_type_id = $pettype->id;
        $objt->owner_id = $owner->id;
        $objt->save();


        
        $objt = new Pet;
        $objt->name = 'Luna';
        $objt->pet_type_id = $pettype->id;
        $objt->owner_id = null;
        $objt->save();


    
    }
}
