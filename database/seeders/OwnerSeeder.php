<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Owner;

class OwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $objt = new Owner;
        $objt->name = " Owner 1";
        $objt->address = "KKKK";
        $objt->phone = "99999";
        $objt->email = "vane@nn.com";
        $objt->comments = "vane@nn.com";
        $objt->save();

        
        $objt = new Owner;
        $objt->name = " Owner 2";
        $objt->address = "KKKK";
        $objt->phone = "99999";
        $objt->email = "vane@nn.com";
        $objt->comments = "vane@nn.com";
        $objt->save();

        
        $objt = new Owner;
        $objt->name = " Owner 3";
        $objt->address = "KKKK";
        $objt->phone = "99999";
        $objt->email = "vane@nn.com";
        $objt->comments = "vane@nn.com";
        $objt->save();

        
        $objt = new Owner;
        $objt->name = " Owner 4";
        $objt->address = "KKKK";
        $objt->phone = "99999";
        $objt->email = "vane@nn.com";
        $objt->comments = "vane@nn.com";
        $objt->save();
    }
}
