<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PetType;

class PetTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objt = new PetType;
        $objt->comments = "RAZA 1";
        $objt->save();

        $objt = new PetType;
        $objt->comments = "RAZA 2";
        $objt->save();

        $objt = new PetType;
        $objt->comments = "RAZA 3";
        $objt->save();

        $objt = new PetType;
        $objt->comments = "RAZA 4";
        $objt->save();
    }
}
