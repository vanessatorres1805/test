<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdoptionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

#http://127.0.0.1:8000/api/pets?owner=1
Route::get('/pets',[AdoptionController::class,'pets']);

#http://127.0.0.1:8000/api/unadoptedpets
Route::get('/unadoptedpets',[AdoptionController::class,'unadoptedpets']);

#http://127.0.0.1:8000/api/numberpetsbytype
Route::get('/numberpetsbytype',[AdoptionController::class,'numberpetsbytype']);

#http://127.0.0.1:8000/api/ownerswithpets
Route::get('/ownerswithpets',[AdoptionController::class,'ownerswithpets']);

#http://127.0.0.1:8000/api/numberpetsbyowner
Route::get('/numberpetsbyowner',[AdoptionController::class,'numberpetsbyowner']);
