## Clonar Repo
git clone https://gitlab.com/vanessatorres1805/test.git

## Configuración 
Crear el archivo .env copiando la estructura del archivo .env.example
Copiar los parametros de conexión según su servidor en el archivo .env

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=test
DB_USERNAME=root
DB_PASSWORD=

## Install
Crear base de datos en mysql con el nombre test
composer install
php artisan migrate
php artisan db:seed

## RUTAS API UTILIZAR EN POSTMAN
#http://127.0.0.1:8000/api/pets?owner=1
#http://127.0.0.1:8000/api/unadoptedpets
#http://127.0.0.1:8000/api/numberpetsbytype
#http://127.0.0.1:8000/api/ownerswithpets
#http://127.0.0.1:8000/api/numberpetsbyowner




## QUERY
● Listar todas las mascotas.
SELECT * FROM `pets`

● Listar las mascotas que no han sido adoptadas.
SELECT * FROM `pets` WHERE `owner_id` IS NULL; 


● Listar el número de mascotas por cada tipo de mascota.
SELECT COUNT(`pets`.`pet_type_id`) AS `pets_count`, `pets_types`.`comments`
FROM `pets_types`
INNER JOIN `pets` ON `pets_types`.`id` = `pets`.`pet_type_id`
GROUP BY `pets`.`pet_type_id`;

● Listar los propietarios que tengan más de una mascota.
SELECT COUNT(`pets`.`owner_id`) AS `pets_count`, `owners`.`name`
FROM `owners`
INNER JOIN `pets` ON `owners`.`id` = `pets`.`owner_id`
GROUP BY `pets`.`owner_id`;

● Listar el número de mascotas por cada tipo de mascota y por cada propietario.
    Son las consultas anteriores


● Listas los propietarios que no tienen mascotas.
SELECT `owners`.`name` as `owners_name`, `pets`.`name` as `pets_name`
FROM `owners`
LEFT JOIN `pets` ON `owners`.`id` = `pets`.`owner_id`
WHERE `pets`.`name` IS NULL;