<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pet;
use App\Models\PetType;
use App\Models\Owner;

class AdoptionController extends Controller
{
    public function index(){
        dd(Pet::all());
    }

    public function pets(Request $request){
        
        if($request->has('owner')){
            $pets = Pet::where('owner_id',$request->owner)->get();
        }else{
            $pets = Pet::all();
        }
        
        return response()->json($pets);
    }

    public function unadoptedpets(){
        $pets = Pet::whereNull('owner_id')->get();
        return response()->json($pets);
    }

    public function numberpetsbytype(){
        $petstypes = PetType::withCount('pets')->get();
        return response()->json($petstypes);
    }

    public function ownerswithpets(){
        $owners = Owner::has('pets')->get();
        return response()->json($owners);
    }

    public function numberpetsbyowner(){
        $owners = Owner::withCount('pets')->get();
        return response()->json($owners);
    }
}
