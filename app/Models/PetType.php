<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PetType extends Model
{
    use HasFactory;
    protected $table = 'pets_types';

    protected $fillable = [
        'id',
        'comments'
    ];

    
    public function pets()
    {
        return $this->hasMany(Pet::class);
    }
}
