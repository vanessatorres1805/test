<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    use HasFactory;

    protected $table = 'pets';

    protected $fillable = [
        'id',
        'name',
        'pet_type_id',
        'owner_id'
    ];

    public function petType()
    {
        return $this->belongsTo(PetType::class,'pet_type_id');
    }

    
    public function owner()
    {
        return $this->belongsTo(Owner::class,'owner_id');
    }


}
