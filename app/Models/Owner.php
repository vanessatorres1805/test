<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    use HasFactory;

    protected $table = 'owners';

    protected $fillable = [
        'id',
        'name',
        'address',
        'phone',
        'email',
        'comments'
    ];

    public function pets()
    {
        return $this->hasMany(Pet::class);
    }

}
